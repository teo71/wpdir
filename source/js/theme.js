"use strict";

/**
 * Создание toast блока (Bootstrap модуль) в DOM со стартовыми настройками *
 * @param message
 */
function create_toast_comment(message) {

    let toast = [
        '<div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">',
        '<div class="toast-header">',
        '<div>Комментарий добавлен</div>',
        '<button type="button" class="close" data-dismiss="toast" aria-label="Close">',
        '<span aria-hidden="true">&times;</span>',
        '</button>',
        '</div>',
        '<div class="toast-body">',
        message,
        '</div>',
        '</div>',
    ].join('');
    $('body').append(toast);
    $('.toast').toast({delay: 7000}).toast('show');
}

/**
 * Копирование нужного текста из html-тега
 * при помощи создания временного элемента input
 * @param text
 * @returns {boolean}
 */
function copy(text) {
    let input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    let result = document.execCommand('copy');
    document.body.removeChild(input);
    return result;
}

$(function () {
    // right sidebar
    $('.post-toc-button>button').click(function () {
        // если ширина экрана < xl -> v.1
        if (screen.width < 1200) {
            $('.navbar-collapse').removeClass('navbar-mobile');
            $('.post-sidebar').toggleClass('open');
        }
        // если ширина экрана >= xl -> v.2
        else {
            if ($.cookie('sidebar_right') === undefined) {
                $.cookie('sidebar_right', 'invisible', {expires: 365, path: '/'});
            } else {
                $.removeCookie('sidebar_right', { path: '/' });
            }
            $('.post-sidebar').toggleClass('sb-none');
        }
    });
    // right sidebar close button
    $('.post-sidebar button.close').click(function () {
        $('.post-sidebar').toggleClass('open');
    });

    // content container resizer
    $('.fullscreen').click(function () {
        if ($.cookie('fullscreen') === undefined) {
            $.cookie('fullscreen', '0', {expires: 365, path: '/'});
            $('main>div').removeClass('container-fluid').addClass('container-xxl');
            $('.fullscreen>i').text('fullscreen');
        } else {
            $.removeCookie('fullscreen', { path: '/' });
            $('main>div').removeClass('container-xxl').addClass('container-fluid');
            $('.fullscreen>i').text('fullscreen_exit');
        }
        return false;
    });

    $('.agreement-footer > button, .agreement-footer .btn').click(function () {
        $.cookie('agreement_footer', '1', { expires: 365, path: '/' });
        $(this).parent().remove();
        return false;
    });

    // change h2 color animation
    $(".table_of_contents a").click(function () {

        let id_val = $(this).attr('href');
        id_val = id_val.replace('#', '');
        const h2_elem = $('h2[id=' + id_val + ']');
        const h3_elem = $('h3[id=' + id_val + ']');

        if (h2_elem.length) {
            h2_elem.addClass('animate');
            // auto remove 'animate' class after end of css animation
            h2_elem.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                h2_elem.removeClass('animate');
            });
        } else {
            h3_elem.addClass('animate');
            // auto remove 'animate' class after end of css animation
            h3_elem.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                h2_elem.removeClass('animate');
            });
        }
    });

    // left sidebar mobile menu // Open
    $('.menu-toggle>button').click(function () {
        $('.post-sidebar').removeClass('open'); // close sidebar at first
        $('.navbar-collapse').toggleClass('navbar-mobile'); //
    });

    // left sidebar mobile menu // Close
    $('.navbar-collapse>.close').click(function () {
        $('.navbar-collapse').toggleClass('navbar-mobile');
    });

    // search form submit
    $('#search-form>.search-button').click(function () {
        $('#search-form').submit();
    });

    // TOC mobile menu
/*    $('.toc-menu-toggle>a').on("click", function () {
        $('.post-sidebar-container').toggleClass('open');
        $('.post-sidebar-container .post-sidebar-container-body').css('position', 'static');
        return false;
    });*/

    // mobile search form
    $('.search-toggle>button').on("click", function () {
        $('.navbar').toggleClass('navbar-search');
        $('#search-form').toggleClass('search-form-mobile');
        $('#search-form>input').focus();

        let search_icon = $(this).children();
        search_icon.html() === "search" ? search_icon.html('close') : search_icon.html('search');

        return false;
    });

    // close all mobile menu
    //$('.post, .cat, .page, .download').on("click", function () {
        // $('body').removeClass();
        // leftmenu
        // $('.post').removeClass('post-mobile');
        // $('.sidebar').removeClass('sidebar-mobile');
        // topmenu
        // $('.navbar-collapse').removeClass('navbar-mobile');
        // $('.navbar-collapse .dropdown-menu').removeClass('show');
        // TOC
        // $('.post-sidebar').removeClass('open');
    //});

    // toast message after add comment
    if (document.location.hash === '#respond') {
        create_toast_comment('Ваш комментарий успешно добавлен и будет опубликован после модерации');
    }

    // comment form title changer
/*    $('.comment-reply-link').click(function () {
        $('#respond .card-header').html(function () {
            return $(this).html().replace('Добавить комментарий', 'Добавить ответ');
        });
    });
    $('#cancel-comment-reply-link').click(function () {
        $('#respond .card-header').html(function () {
            return $(this).html().replace('Добавить ответ', 'Добавить комментарий');
        });
    });*/

    /**
     * Popover
     */
    let myDefaultWhiteList = $.fn.popover.Constructor.Default.whiteList;
    myDefaultWhiteList.form = [];
    myDefaultWhiteList.input = ['type', 'value'];

/*    $('body').click(function (e) {
        // hide left mobile sidebar
        let navbarMobile = $('header>nav>.navbar-mobile');
        console.log(e.target.tagName);
        if (!$(e.target).parent().hasClass('btn-salad')) {
            navbarMobile.toggleClass('navbar-mobile');
        }
    });*/

    // hide popovers
    $('article').click(function () {
        // hide all popovers
        if ($('body>.popover').length)
            $('a.int').popover('hide');
    });

    // popover init + event handlers
    $('a.int')
        .popover({
            trigger: 'manual',
            placement: 'top',
            html: true,
            title: '<span>Откройте адрес в новой вкладке</span><i class="material-icons">close</i>'
        })
        .on('show.bs.popover', function () {
            let data_content = $(this).attr('href');
            data_content = '<form><input class=\'form-control form-control-sm\' type=\'text\' value=\'' + data_content + '\'/></form>';
            data_content += '<a href="#"><i class="material-icons">content_copy</i></a>';
            $(this).attr('data-content', data_content);
        })
        .mouseenter(function () {
            $('a.int').popover('hide');
            $(this).popover('show');
        });

    // popover internal html
    $('body')
        .on("click", ".popover-body>a", function (e) {
            if (copy($(this).prev().children().val())) {
                // перекрашиваем иконку, если текст скопирован в буфер
                $(this).addClass('text-success');
                $(this).parent().prev().find('span').addClass('text-success').text('URL-адрес успешно скопирован...');
            }
            return false;
        })
        .on("click", ".popover-body>form>input", function () {
            if (copy($(this).val())) {
                // перекрашиваем иконку, если текст скопирован в буфер
                $(this).parent().parent().prev().find('span').addClass('text-success').text('URL-адрес успешно скопирован...');
            }
            $(this).select();
            return false;
        })
        .on("click", ".popover-header>i", function () {
            $('a.int').popover('hide');
            return false;
        });
    /**
     * Popover
     */

    /**
     * Scroll UP
      */
    // стартовый показ/нет элемента
    let mql = window.matchMedia("(min-width: 768px)");
    mql.addEventListener("change", (e) => {
        if (e.matches) {
            if (window.pageYOffset > 400) {
                $('.scroll-up').show();
            } else {
                $('.scroll-up').hide();
            }
        } else {
            $('.scroll-up').hide();
        }
    });

    // показ элемента после 400px прокрутки вниз
    $(window).scroll(function () {
        if (mql.matches) {
            if (this.pageYOffset > 400) {
                $('.scroll-up').show();
            } else {
                $('.scroll-up').hide();
            }
        }
    });

    // скролл страницы вверх
    $('.scroll-up > button').click(function (e) {
        e.preventDefault();
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });
    /**
     * Scroll UP
     */
});