'use strict';

const site_domain = "local.wp_domain_name";

const source_dir = 'source';
const theme_dir = 'public/wp-content/themes/wp_theme_name';
const plugins_dir = 'public/wp-content/plugins';

const gulp = require('gulp');
const sass = require('gulp-sass');
const bs = require('browser-sync').create();
const csso = require('gulp-csso');
const rename = require("gulp-rename");
const babel = require('gulp-babel');
const uglify = require("gulp-uglify");

// scss >> css >> theme.min.css
gulp.task('sass', function () {
    return gulp.src([source_dir + '/scss/**/*.scss'])
        .pipe(sass())
        .pipe(csso())
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(theme_dir + '/css'));
});
// theme js >> min.js
gulp.task('minjs-theme', function () {
    return gulp.src([source_dir + '/js/*.js'])
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(uglify())
        .pipe(rename({extname: '.min.js'}))
        .pipe(gulp.dest(theme_dir + '/js'));
});

// WP PLR plugin js >> min.js
gulp.task('minjs-plr', function () {
    let plr_js_dir = plugins_dir + '/post-like-rating/js/';
    return gulp.src([plr_js_dir + 'plr.js', '!' + plr_js_dir + '*.min.js'])
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(uglify())
        .pipe(rename({extname: '.min.js'}))
        .pipe(gulp.dest(plr_js_dir));
});

// proxy server
gulp.task('sync', function () {
    bs.init({
        proxy: site_domain,
        open: false,
        scrollRestoreTechnique: 'cookie'
    });

    bs.watch([theme_dir + '/js/*.js', theme_dir + '/css/*.css', theme_dir + '/**/*.php', theme_dir + '/*.php'], function (event, file) {
        if (event === "change") {
            bs.reload();
        }
    });
});

// watchers [*.scss, ]
gulp.task('watch', function () {
    gulp.watch(source_dir + '/**/*.scss', gulp.series('sass'));
    gulp.watch(source_dir + '/js/*.js', gulp.series('minjs-theme'));
});

gulp.task('default', gulp.series('sass', 'minjs-theme', gulp.parallel('watch', 'sync')));