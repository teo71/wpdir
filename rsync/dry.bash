#!/bin/bash

domain="wp_domain_name"
domain_local="local.wp_domain_name"

username="wp_domain"
usergroup="wp_domain"

ip="x.x.x.x"

# rsync dry launch
# '-n' parameter for trial launch
rsync -avhn --delete --include-from=rsync/include --exclude-from=rsync/exclude --chmod=D775,F664 -og --chown="${username}:${usergroup}" -e "ssh -p 2288" "/q/www/${domain_local}/" "${username}@${ip}:~/www/${domain}/"