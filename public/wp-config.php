<?php

$domain_name = 'wp_domain_name';

/** Load DB config **/
if ( file_exists( dirname( __FILE__ ) . '/../live-config.php' ) ) {
	define( 'WP_LOCAL_DEV', false );
	include( dirname( __FILE__ ) . '/../live-config.php' );
}
else {
	define( 'WP_LOCAL_DEV', true );
	include( dirname( __FILE__ ) . '/../local-config.php' );

	/** added 'local' subdomain for develor site version **/
	$domain_name = 'local.' . $domain_name;
}
//echo $domain_name;

/** Custom Content Directory **/
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
define( 'WP_CONTENT_URL', 'http://' . $domain_name . '/wp-content' );

/** Disable automatic updates **/
define( 'AUTOMATIC_UPDATER_DISABLED', true );

/** Post Revision Limit **/
define( 'WP_POST_REVISIONS', 10 );

/** Disable Default Cron Manager **/
define( 'DISABLE_WP_CRON', true );

// =======================
// Load WordPress Settings
// =======================
$table_prefix = 'wp_db_table_prefix';

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
}
require_once( ABSPATH . 'wp-settings.php' );