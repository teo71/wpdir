<div class="card short">
    <div class="card-body">
        <a href="<?php the_permalink(); ?>">
			<?php the_title(); ?>
        </a>
        <div class="card-text">
			<?php echo strip_tags( get_the_content( '' ) ); ?>
        </div>
    </div>
    <div class="card-footer text-muted">
        <div class="row">
            <?php
            // выводим значение рейтинга только, если он не нулевой
            $plr_short_rating_text  = plr_short_rating();
            $plr_short_rating_value = intval( strip_tags( $plr_short_rating_text ) );
            if ( $plr_short_rating_value ):
            ?>
            <div class="col-auto rating" title="Рейтинг публикации">
                <i class="material-icons">thumb_up_alt</i><?php echo $plr_short_rating_text; ?>
            </div>
            <?php endif; ?>
	        <?php if ( get_comments_number() ): ?>
            <div class="col-auto comment" title="Комментарии к статье">
                <a href="<?php the_permalink(); ?>#comments"><i class="material-icons">textsms</i></a>
                <?php echo comments_number('', '<span>1</span>', '<span>%</span>'); ?>
            </div>
            <?php endif; ?>
            <!--<div class="col-auto video" title="Видео версия статьи на YouTube">
                <a href="">
                    <i class="material-icons">videocam</i>
                </a>
            </div>-->
            <div class="col text-right">
                <a href="<?php the_permalink(); ?>" class="btn btn-link btn-sm">Подробнее</a>
            </div>
        </div>
    </div>
</div>