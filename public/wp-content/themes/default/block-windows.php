<h1><?php echo set_document_custom_h1_content( get_the_title() ); ?></h1>
<?php the_content(); ?>

<?php
// данные из кеша (tools.php)
$chromeURLParts = wp_cache_get( 'chromeURLParts' );

// получение ссылок текущей страницы
$dl_links = get_fields(22);
?>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tbody>
                <tr>
                    <td>
                        <div class="text-success">Стабильная</div>
                        <div class="d-none d-xl-block">Рекомендуемая к установке для большинства пользователей версия</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'stable', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'stable', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-warning">Beta</div>
                        <div class="d-none d-xl-block">Если желаете проверить функции, которые скоро появятся в стабильной ветке</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'beta', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'beta', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-danger">Developer</div>
                        <div class="d-none d-xl-block">Возможность теста самых свежих идей и, разумеется, багов будущих версий приложения</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'developer', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'developer', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>Canary</div>
                        <div class="d-none d-xl-block">Ещё более ранние изменения, чем в Developer. Имеет собственный канал обновлений.</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'canary', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
									<?php echo buildChromeDownloadURL( $chromeURLParts, 'canary', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-success">Старые версии</div>
                        <div class="d-none d-xl-block">архив предыдущих выпусков дистрибутивов браузера под Windows</div>
                    </td>
                    <td>
                        <a class="btn btn-outline-secondary" href="<?php echo $dl_links['win_archive']; ?>" rel="nofollow noopener noreferrer">
                            <i class="material-icons">get_app</i>открыть архив
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>