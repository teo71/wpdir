<div class="card">
    <div class="card-header">
        Результаты поиска
    </div>
    <div class="card-body">
        <div class="search-box">
            <form method="get" action="/">
                <div class="form-row">
                    <div class="col">
                        <input name="s" class="form-control" type="search" value="<?php echo get_search_query(); ?>" placeholder="Ваш поисковый запрос...">
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-darkblue">Найти</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
// кол-во символов запроса
$search_query_length = mb_strlen( get_search_query(), 'utf-8' );
if ( $search_query_length >= 3 ):
?>
<div class="search-results">
    По вашему запросу найдено ответов: <?php echo custom_search_results_count(); ?>
</div>
<?php else: ?>
<div class="alert alert-warning font-weight-light mt-3">
    <span>Слишком короткий поисковый запрос. Необходимо минимум 3 символа.</span>
</div>
<?php endif; ?>
