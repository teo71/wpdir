<footer class="bg-dark text-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-auto">
                Советник Google Chrome
            </div>
            <div class="col-auto ml-auto">
                <!--<div class="counter"></div>-->
            </div>
        </div>
    </div>
    <div class="scroll-up">
        <button>
            <i class="material-icons">arrow_circle_up</i>
        </button>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
