<div class="panel">
    <div class="panel-header">
        <h1><?php echo (is_category(17)) ? 'KMPlayer ' : ''; ?><?php the_title(); ?></h1>
    </div>
    <div class="panel-body">
		<?php $more = 0; ?>
		<?php the_content( null, true ); ?>
        <?php
		if ( is_page( 'android' ) AND is_readable( get_template_directory() . '/inc/android.php' ) ) {
			include 'inc/android.php';
		}
        elseif ( is_page( [ 'windows', 'russian', 'torrent' ] ) AND is_readable( get_template_directory() . '/inc/windows.php' ) ) {
			include 'inc/windows.php';
		}
		elseif ( is_page( 'portable' ) AND is_readable( get_template_directory() . '/inc/portable.php' ) ) {
			include 'inc/portable.php';
		}
        elseif ( is_page( 'help' ) AND is_readable( get_template_directory() . '/inc/help.php' ) ) {
			include 'inc/help.php';
		}
		elseif ( is_page( 'version' ) AND is_readable( get_template_directory() . '/inc/version.php' ) ) {
			include 'inc/version.php';
		}
        ?>
    </div>
</div>