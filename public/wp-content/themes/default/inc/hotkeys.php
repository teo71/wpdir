<table class="table table-bordered table-hotkeys">
    <thead>
        <tr>
            <th>Функция</th>
            <th>Сочетание клавиш</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Открыть новую вкладку</td>
            <td><span class="badge badge-info">Ctrl + T</span> (обычную) или <span class="badge badge-info">Ctrl + Shift + N</span> (в режиме <a href="">инкогнито</a>)</td>
        </tr>
        <tr>
            <td>Закрыть активную вкладку</td>
            <td><span class="badge badge-info">Ctrl + W</span></td>
        </tr>
        <tr>
            <td>Обновить страницу</td>
            <td><span class="badge badge-info">F5</span> или <span class="badge badge-info">Ctrl + F5</span> (со сбросом <a href="">кэша</a>)</td>
        </tr>
        <tr>
            <td>Навигация по странице</td>
            <td><span class="badge badge-info">Home</span> (в начало), <span class="badge badge-info">End</span> (в конец)</td>
        </tr>
        <tr>
            <td><a href="/search-by-page/">Поиск по странице</a></td>
            <td><span class="badge badge-info">Ctrl + F</span></td>
        </tr>
        <tr>
            <td>Интернет поиск</td>
            <td><span class="badge badge-info">Ctrl + E</span></td>
        </tr>
        <tr>
            <td>
                <a href="/downloads/">Список загрузок</a>
            </td>
            <td><span class="badge badge-info">Ctrl + J</span></td>
        </tr>
        <tr>
            <td><a href="/boomarks/">Создать закладку</a></td>
            <td><span class="badge badge-info">Ctrl + D</span></td>
        </tr>
        <tr>
            <td>
                <a href="/page-scale/">Масштаб страницы</a>
            </td>
            <td><span class="badge badge-info">Ctrl + Плюс</span> (увеличить) или <span class="badge badge-info">Ctrl + Минус</span> (уменьшить)</td>
        </tr>
        <tr>
            <td>
                <a href="/reading-mode/">Режим чтения</a>
            </td>
            <td><span class="badge badge-info">Alt + B</span></td>
        </tr>
    </tbody>
</table>