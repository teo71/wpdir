<?php

include_once 'functions/tools.php';
include_once 'functions/disable.php';
include_once 'functions/content.php';
include_once 'functions/seo.php';
include_once 'functions/admin.php';
include_once 'functions/wiki.php';
include_once 'functions/search.php';
include_once 'functions/comments.php';

/**
 * Подключение библиотек js/css:
 * 1) jQuery Slim
 * 2) Bootstrap (+ Popper)
 * 3) Web font 'Roboto': 300, 400, 500 + cyrillic
 * 4) Web font 'Material Icons'
 * 5) Gallery FancyBox
 * 6) WP default 'comment-reply.js'
 */
add_action( 'wp_enqueue_scripts', function () {

	// отключение встроенной jQuery
	wp_deregister_script( 'jquery' );

	// добавление случайного GET параметра для отключения кеширования, если DEBUG включен
	$randVer = ( true === WP_DEBUG ) ? time() : null;

	// external font roboto
	wp_enqueue_style( 'roboto', '//fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=cyrillic', [], null );
	wp_enqueue_style( 'poppins', '//fonts.googleapis.com/css?family=Poppins:400&display=swap', [], null );

	// регистрация отдельной версии jQuery под Bootstrap
	wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', [], null );

	// подключение Bootstrap скриптов
	wp_enqueue_script( 'popper', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', [ 'jquery' ], null );
	wp_enqueue_script( 'bs', '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js', [ 'jquery' ], null );

	// подключение своего js скрипта
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/theme.min.js', [ 'jquery' ], $randVer );

	// Meterial Design Icons Web Font
	wp_enqueue_style( 'md', '//fonts.googleapis.com/icon?family=Material+Icons', [], $randVer );

	if ( is_singular( 'post' ) ) {
		// FancyBox Img Popup/Gallery Script
		wp_enqueue_style( 'fbcss', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css', [], $randVer );
		wp_enqueue_script( 'fbjs', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js', [ 'jquery' ], null );
	}

	// подключение Bootstrap темы
	wp_enqueue_style( 'bs-theme', get_template_directory_uri() . '/css/theme.min.css', [], $randVer );

	// jquery.cookie plugin
	wp_enqueue_script( 'jqjs', '//cdn.jsdelivr.net/npm/jquery.cookie@1.4.1/jquery.cookie.min.js', [ 'jquery' ], null );

	// comment reply form
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply-custom', '/wp-includes/js/comment-reply.js', [ 'jquery' ], null, true );
	}

	// animate library
	wp_enqueue_style( 'animate_style', '//cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css', [], $randVer );
} );

/**
 * Вывод предупреждения в футере об использовании cookie-файлов на сайте
 */
function add_footer_agreement_line() {
	if ( ! isset( $_COOKIE['agreement_footer'] ) ) {
?>
<div class="agreement-footer">
	<span>
		Просматривая материалы сайта, вы даёте своё согласие на <a href="/privacy-policy/#cookie">использование</a> файлов cookie.
	</span>
	<a href="#" class="btn btn-sm btn-outline-secondary">Принять</a>
	<button type="button" class="close d-none d-lg-block" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button>
</div>
<?php
	}
}
add_action( 'wp_footer', 'add_footer_agreement_line' );