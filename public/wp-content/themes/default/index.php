<?php

if ( WP_COMINGSOON AND ! current_user_can( 'edit_posts' ) ) {
	include( 'coming-soon.php' );
} else {
	include( 'main.php' );
}