<div class="row">
    <div class="col-12 col-lg">
        <h1><?php the_title(); ?></h1>
		<?php $more = 0; ?>
		<?php the_content( null, true ); ?>
    </div>
	<?php if ( is_front_page() ): ?>
        <div class="col-12 col-lg-auto home-sidebar">
            <div class="card">
                <div class="card-header">
                    Рекомендуем
                    <i class="material-icons">emoji_people</i>
                </div>
                <div class="card-body">
	                <?php echo get_main_sidebar_posts('recommended'); ?>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Интересное
                    <i class="material-icons">directions_run</i>
                </div>
                <div class="card-body">
	                <?php echo get_main_sidebar_posts('interesting'); ?>
                </div>
            </div>

            <?php
            $last_comments = get_last_comments();
            if ( ! empty( $last_comments ) ):
            ?>
            <div class="card lastcomments">
                <div class="card-header">
                    Комментарии
                    <i class="material-icons">sms</i>
                </div>
                <div class="card-body">
                    <?php echo $last_comments; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
	<?php endif; ?>
</div>