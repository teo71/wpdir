<?php get_header(); ?>
<?php
// установка ширины контейнера контента
// в зависмости от cookie 'fullscreen' (0 - xxl, undefined - fluid)
$content_container = ( ! isset( $_COOKIE['fullscreen'] ) ) ? 'fluid' : 'xxl';
?>
    <main>
        <div class="container-<?php echo $content_container; ?>">
			<?php

			if ( is_category() ) {
				$col_class = 'cat';
			} elseif ( is_page( 'download' ) ) {
				$col_class = 'download';
			} elseif ( is_page( 'android' ) ) {
				$col_class = 'download android';
			} elseif ( is_page( 'portable' ) ) {
				$col_class = 'download portable';
			} elseif ( is_page( 'help' ) ) {
				$col_class = 'help';
			} elseif ( is_page( 'windows' ) ) {
				$col_class = 'download windows';
			} elseif ( is_page() ) {
				// для всех остальных страниц
				$col_class = 'page';
			} elseif ( is_search() ) {
				$col_class = 'search';
			} elseif(is_singular('post')) {
				$col_class = 'post';
			} else {
				$col_class = '';
			}

			?>
            <div class="<?php echo $col_class; ?>">

				<?php
				// Начала страницы результатов поиска
				$search_results_output = true;
				if ( is_search() ) {
					// кол-во символов запроса
					$search_query_length = mb_strlen( get_search_query(), 'utf-8' );
					get_template_part( 'search', 'header' );
					if ( custom_search_results_count() > 0 AND $search_query_length >= 3 ) {
						echo '<ul>';
					}
					else {
						// чтобы не делать запрос в базу при коротком поисковом запросе
						$search_results_output = false;
					}
				}
				// category title + breadcrumbs
				if ( is_category() ) {
					get_template_part( 'block', 'category' );
				}
				?>

				<?php if ( have_posts() AND ! is_404() AND $search_results_output ): ?>
					<?php
					$loop_position = 0;
					while ( have_posts() ): the_post();

						$loop_position++;

						// wiki page
						if ( is_page( 'help' ) ) {
							get_template_part( 'block', 'help' );
						}
						// portable
                        elseif ( is_page( 'portable' ) ) {
							get_template_part( 'block', 'portable' );
						}
						// download
                        elseif ( is_page( 'download' ) ) {
							get_template_part( 'block', 'download' );
						}
						// android
                        elseif ( is_page( 'android' ) ) {
							get_template_part( 'block', 'android' );
						}
						// windows
                        elseif ( is_page( 'windows' ) ) {
							get_template_part( 'block', 'windows' );
						}
                        elseif ( is_page() ) {
							get_template_part( 'block', 'page' );
						}
                        elseif ( is_single() ) {
							get_template_part( 'block', 'post' );
						}
                        elseif ( is_category() ) {
							get_template_part( 'block', 'shortstory' );
						}
                        elseif ( is_search() AND $search_query_length >= 3 ) {
							if ( custom_search_results_count() > 0 ) {
								get_template_part( 'search', 'shortstory' );
							}
						}
					endwhile;
					?>
				<?php elseif ( is_404() ): ?>
					<?php
					get_template_part( 'block', '404' );
					?>
				<?php endif; ?>

				<?php
				// пагинация в категории
				if ( is_category() ) {
					bootstrap_pagination();
				}
				?>

				<?php
				// Конец страницы результатов поиска
				if ( is_search() ) {
					if ( custom_search_results_count() > 0 AND $search_query_length >= 3 ) {
						echo '</ul>';
					}
				}
				?>

				<?php
				// комментрии для post type = 'page'
				if ( is_page() ) {
					if ( comments_open() || '0' != get_comments_number() ) {
						comments_template();
					}
				}
				?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>