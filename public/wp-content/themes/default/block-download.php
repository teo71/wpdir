<h1><?php the_title(); ?></h1>
<?php the_content(); ?>

<?php
// данные из кеша (tools.php)
$chromeURLParts = wp_cache_get( 'chromeURLParts' );

// получение ссылок текущей страницы
$dl_links = get_fields(22);
?>
<div class="card">
    <div class="card-header">
        Версии для Windows
    </div>
    <div class="card-body">
        <table class="table">
            <tbody>
                <tr>
                    <td>
                        <div class="text-success">Стабильная <sup>2</sup></div>
                        <div class="d-none d-xl-block">Рекомендуемая к установке для большинства пользователей версия</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'stable', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'stable', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-warning">Beta</div>
                        <div class="d-none d-xl-block">Если желаете проверить функции, которые скоро появятся в стабильной ветке</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'beta', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'beta', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-danger">Developer</div>
                        <div class="d-none d-xl-block">Возможность теста самых свежих идей и, разумеется, багов будущих версий приложения</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'developer', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'developer', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>Canary <sup>3</sup></div>
                        <div class="d-none d-xl-block">Ещё более ранние изменения, чем в Developer. Имеет собственный канал обновлений.</div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x64
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'canary', '64' ); ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-offset="0,3" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">get_app</i>x32
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Типы дистрибутивов</div>
                                    <div class="dropdown-divider"></div>
				                    <?php echo buildChromeDownloadURL( $chromeURLParts, 'canary', '32' ); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        Файлы для других ОС
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a class="nav-link active" id="linux-tab" data-toggle="tab" href="#linux" role="tab">Linux <sup>4</sup></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="android-tab" href="/android/" role="tab">Android</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="linux-tab" data-toggle="tab" href="#ios" role="tab">iOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="macos-tab" data-toggle="tab" href="#macos" role="tab">Mac OS</a>
            </li>
        </ul>
    </div>
    <div class="card-body tab-content p-0" id="myTabContent">
        <div class="tab-pane fade active show" id="linux" role="tabpanel" aria-labelledby="linux-tab">
            <table class="table">
                <tbody>
                    <tr>
                        <td>
                            <div class="text-success">Стабильная</div>
                        </td>
                        <td>
                            <a href="<?php echo $dl_links['linux_stable_deb']; ?>" class="btn btn-outline-secondary" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.deb
                            </a><a href="<?php echo $dl_links['linux_stable_rpm']; ?>" class="btn btn-outline-secondary" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.rpm
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-warning">Beta</div>
                        </td>
                        <td>
                            <a href="<?php echo $dl_links['linux_beta_deb']; ?>" class="btn btn-outline-secondary" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.deb
                            </a><a href="<?php echo $dl_links['linux_beta_rpm']; ?>" class="btn btn-outline-secondary" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.rpm
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-danger">Developer</div>
                        </td>
                        <td>
                            <a href="<?php echo $dl_links['linux_dev_deb']; ?>" class="btn btn-outline-secondary" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.deb
                            </a><a href="<?php echo $dl_links['linux_dev_rpm']; ?>" class="btn btn-outline-secondary" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.rpm
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="ios" role="tabpanel" aria-labelledby="ios-tab">
            <table class="table mb-0">
                <tbody>
                    <tr>
                        <td>
                            <div class="text-success">Стабильная</div>
                        </td>
                        <td>
                            <a href="<?php echo $dl_links['ios']; ?>" class="btn btn-outline-secondary" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>Скачать
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="macos" role="tabpanel" aria-labelledby="macos-tab">
            <table class="table mb-0">
                <tbody>
                    <tr>
                        <td>
                            <div class="text-success">Стабильная</div>
                        </td>
                        <td>
                            <a class="btn btn-outline-secondary" href="<?php echo $dl_links['macos_stable']; ?>" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.dmg
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-warning">Beta</div>
                        </td>
                        <td>
                            <a class="btn btn-outline-secondary" href="<?php echo $dl_links['macos_beta']; ?>" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.dmg
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-danger">Developer</div>
                        </td>
                        <td>
                            <a class="btn btn-outline-secondary" href="<?php echo $dl_links['macos_developer']; ?>" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.dmg
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div>Canary</div>
                        </td>
                        <td>
                            <a class="btn btn-outline-secondary" href="<?php echo $dl_links['macos_canary']; ?>" rel="nofollow noreferrer">
                                <i class="material-icons">get_app</i>.dmg
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<ol class="footnote">
    <li>для Windows мы даём 4 варианты ссылок - 2 прямых (полная версия и загрузчик) и 2 таких же на официальный сайт</li>
    <li>для Windows XP/Vista вам нужна <a href="/windows/">архивная</a> версия 49.0.2623.112 или любая более ранняя</li>
    <li>для ветки Canary доступны только web-установщик</li>
    <li>для Linux существуют только 64-битные варианты дистрибутивов</li>
</ol>
