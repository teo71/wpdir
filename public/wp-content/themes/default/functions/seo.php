<?php

/**
 * включение возможности формирования title
 */
add_theme_support( 'title-tag' );

/**
 * Формирование содержимого <title></title>
 *
 * @param $title string
 *
 * @return string
 */
function set_document_title( $title ) {

	if ( is_page() OR is_single() ) {
		// получение ACF полей текущего поста (страницы)
		$get_current_post_fields = get_current_post_ACF_fields();

		$title = ( ! empty( $get_current_post_fields['seo-title'] ) ) ? wp_strip_all_tags( $get_current_post_fields['seo-title'], true ) : $title;
	}
	elseif ( is_category() ) {
		// получение ACF полей текущей категории
		$get_current_post_fields = get_current_post_ACF_fields( 'term_' . get_queried_object()->term_id );

		$title = ( ! empty( $get_current_post_fields['seo-title'] ) ) ? wp_strip_all_tags( $get_current_post_fields['seo-title'], true ) : $title;
	}

	return $title;
}
add_filter( 'pre_get_document_title', 'set_document_title' );

/**
 * Формирование содержимого тега H1 из поля ACF
 *
 * @param $h1_text string
 *
 * @return string
 */
function set_document_custom_h1_content( $h1_text ) {

	if ( ( is_page() OR is_single() ) AND ! is_front_page() ) {
		// получение ACF полей текущего поста (страницы)
		$get_current_post_fields = get_current_post_ACF_fields();
		$h1_text = ( ! empty( $get_current_post_fields['seo-h1'] ) ) ? wp_strip_all_tags( $get_current_post_fields['seo-h1'], true ) : $h1_text;
	}

	return $h1_text;
}

/**
 * Вывод мета тега "description" для post, page, cat:
 */
function add_meta_description() {

	if ( is_page() OR is_singular( 'post' ) ) {
		// получение ACF полей текущего поста (страницы)
		$get_current_post_fields = get_current_post_ACF_fields();
		$description = ( ! empty( $get_current_post_fields['seo-descr'] ) ) ? $get_current_post_fields['seo-descr'] : false;
	}
	elseif ( is_category() ) {
		// получение ACF полей текущей категории
		$get_current_post_fields = get_current_post_ACF_fields( 'term_' . get_queried_object()->term_id );
		$description = ( ! empty( $get_current_post_fields['seo-descr'] ) ) ? $get_current_post_fields['seo-descr'] : false;
	}

	if ( ! empty( $description ) ) {
		echo '<meta name="description" content="' . esc_attr( $description ) . '">' . PHP_EOL;
	}
}
add_action( 'wp_head', 'add_meta_description' );

/**
 * Исправление бага(?) WP, при котором урлы вида /test-post.html и /test.post.html одинаково отдают 200 ответ
 */
add_filter( 'request', function ( $query_vars ) {

	if ( isset( $query_vars['name'] ) AND strpos( $query_vars['name'], '.' ) !== false ) {
		wp_redirect( trailingslashit( home_url() ) . str_replace( '.', '-', $query_vars['name'] ) . '.html', 301 );
	}

	return $query_vars;
} );

/**
 * Отключение индексации страниц пагинации, результатов поиска
 */
function meta_robots_creator() {

	// результаты поиска
	if ( is_search() ) {
		echo '<meta name="robots" content="noindex,nofollow">' . PHP_EOL;
	} // пагинация
	elseif ( is_paged() ) {
		echo '<meta name="robots" content="noindex,follow">' . PHP_EOL;
	} // страницы "контакты", "о проекте" и "политика конфиденциальности"
	elseif ( is_page( [ 'about', 'privacy-policy' ] ) ) {
		echo '<meta name="robots" content="noindex,follow">' . PHP_EOL;
	}

}
add_action( 'wp_head', 'meta_robots_creator' );

/**
 * Вывод правильного блока favicon
 * Сгенерирован сервисом https://realfavicongenerator.net
 */
function favicon_block_generator() {

	echo <<<HTML
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#6c767e">
<meta name="theme-color" content="#ffffff">
HTML;

	echo PHP_EOL;
}
add_action( 'wp_head', 'favicon_block_generator' );

/**
 * Собственный вариант robots.txt
 *
 * @param $output
 *
 * @return string
 */
function custom_robots_txt( $output ) {

	$site = wp_parse_url( home_url() );

	$output .= 'Sitemap: ' . $site['scheme'] . '://' . $site['host'] . '/sitemap.xml' . PHP_EOL;
	$output .= PHP_EOL;

	return $output;
}
add_action( 'robots_txt', 'custom_robots_txt' );