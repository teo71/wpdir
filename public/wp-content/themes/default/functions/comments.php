<?php

/**
 * Вывод N последних комментариев сайта
 *
 * @param int $number количество комментариев
 *
 * @return array|string
 */
function get_last_comments( $number = 3 ) {

	$number = intval($number);
	$args = [
		'number'  => ($number) ? $number : 3,
		'orderby' => 'comment_date',
		'order'   => 'DESC',
		'status'  => 'approve',
		'type'    => 'comment',
	];

	$last_comments_ul = [];
	$last_comments = get_comments( $args );
	if ( ! empty( $last_comments ) ) {
		foreach ( $last_comments as $comment ) {

			$comment_link  = get_comment_link( $comment->comment_ID );
			$author_avatar = get_avatar( $comment->comment_author_email, 32 );

			$comment_content = mb_substr( strip_tags( $comment->comment_content ), 0, 50, 'utf-8' );
			$comment_content = ( mb_strlen( $comment->comment_content, 'utf-8' ) > 50 ) ? $comment_content . '...' : $comment_content;

			$last_comments_ul[] = <<<HTML
				<li>
					<div class="comm-author">
						{$author_avatar}
						<span>{$comment->comment_author}</span>	
					</div>
					<a href="{$comment_link}">
						{$comment_content}
					</a>
				</li>								
HTML;
		}
		$last_comments_ul = '<ul>' . implode( '', $last_comments_ul ) . '</ul>';
	}

	$last_comments_ul = ( ! empty( $last_comments_ul ) ) ? $last_comments_ul : '';

	return $last_comments_ul;
}

/**
 * Изменение стандартного редиректа после добавления комментария, если включена их премодерация
 *
 * @param $location
 * @param $comment
 *
 * @return string
 */
function comment_redirect_after_added( $location, $comment ) {

	return get_permalink( $comment->comment_post_ID ) . '#respond';
}
add_filter( 'comment_post_redirect', 'comment_redirect_after_added', 10, 2 );

/**
 * Изменение порядка вывода полей формы комментирования
 * Поле textarea перемещается в конец списка
 */
function comment_form_fields_order( $fields ) {

	// 1. textarea в конец списка
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;

	// 2. убираем поле url, email и checkbox
	unset( $fields['email'] );
	unset( $fields['url'] );
	unset( $fields['cookies'] );

	return $fields;
}
add_filter( 'comment_form_fields', 'comment_form_fields_order' );

/**
 * Добавление аттрибута 'title' со значением 'Ответить'
 * для ссылки ответа на комментарий
 */
function comment_reply_link_title_atribute( $link ) {
	return str_replace( '<a ', '<a title="Ответить" ', $link );
}
add_action( 'comment_reply_link', 'comment_reply_link_title_atribute' );