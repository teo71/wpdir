<?php

/**
 * Вывод кол-ва постов в результатах поиска
 *
 * @return int Кол-во результатов поиска
 */
function custom_search_results_count() {

	global $wp_query;

	return $wp_query->found_posts;
}

/**
 * В результатах пользовательского поиска остаётся только 1 страница
 */
function custom_search_found_posts( $found_posts, $query ) {

	if ( is_search() AND ! is_admin() ) {

		if ( $query->get( 'posts_per_page' ) < $query->found_posts ) {

			$found_posts = $query->get( 'posts_per_page' );
		}
	}

	return $found_posts;
}
add_filter( 'found_posts', 'custom_search_found_posts', 10, 2 );