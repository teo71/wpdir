<?php
/**
 * Отключение ненужных страниц, блоков, функций CMS
 */

//
//  https://wp-kama.ru/question/kak-polnostyu-otklyuchit-rest-api-vvedennyj-v-wp-4-4
//  https://sheensay.ru/wordpress-clean/
//
// Отключаем WP-API версий 1.x
add_filter( 'json_enabled', '__return_false' );
add_filter( 'json_jsonp_enabled', '__return_false' );

// Отключаем WP-API версий 2.x
//add_filter( 'rest_enabled', '__return_false' ); // deprecated
add_filter( 'rest_jsonp_enabled', '__return_false' );

// Удаляем информацию о REST API из заголовков HTTP и секции head
remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'template_redirect', 'rest_output_link_header', 11 );

// Отключаем фильтры REST API
remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
remove_action( 'auth_cookie_malformed', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_expired', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_bad_username', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_bad_hash', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_valid', 'rest_cookie_collect_status' );
remove_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100 );

// Отключаем события REST API
remove_action( 'init', 'rest_api_init' );
remove_action( 'rest_api_init', 'rest_api_default_filters', 10 );
remove_action( 'parse_request', 'rest_api_loaded' );

// Отключаем Embeds связанные с REST API
remove_action( 'rest_api_init', 'wp_oembed_register_route' );
remove_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10 );

// Убираем oembed ссылки в секции head
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

// блокировка вставок с других сайтов
remove_action( 'wp_head', 'wp_oembed_add_host_js' );

// Для редиректа с http://site.ru/wp-json/ на главную
add_action( 'template_redirect', function() {
	if ( preg_match( '#^/wp-json/?$#i', $_SERVER['REQUEST_URI'] ) ) {
		wp_redirect( get_option( 'siteurl' ), 301 );
		exit;
	}
} );

/**
 * Отключение всех типов rss фидов
 * Отключение вывода ссылок в шаблон: https://kinsta.com/knowledgebase/wordpress-disable-rss-feed/
 */
function wpext_rss_block( $is_comment_feed, $feed ) {

	if ( $is_comment_feed ) {
		wp_die( "Данный RSS канал комментариев был заблокирован", 'RSS канал заблокирован', [ 'response' => 404 ] );
	} else {
		wp_die( "Данный RSS канал ({$feed}) был заблокирован", 'RSS канал заблокирован', [ 'response' => 404 ] );
	}
}
add_action( 'do_feed_rdf', 'wpext_rss_block', 1, 2 );
add_action( 'do_feed_rss', 'wpext_rss_block', 1, 2 );
add_action( 'do_feed_rss2', 'wpext_rss_block', 1, 2 );
add_action( 'do_feed_atom', 'wpext_rss_block', 1, 2 );

// Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links_extra', 3 );

// Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'feed_links', 2 );

/**
 * Отключение "архивов" типа /author/_name_ и year|month|day
 * Вывод постов таксономии тоже архив, но мы их пропускаем
 */
function wpext_archives_disable( $preempt ) {

	if ( is_author() OR is_date() OR is_attachment() ) {

		global $wp_query;

		$wp_query->set_404();
		status_header( 404 );
		nocache_headers();

		return true; // для обрыва хука
	}

	return $preempt;
}
add_filter( 'pre_handle_404', 'wpext_archives_disable' );

/**
 * Отключение DNS prefetch для emoji
 */
add_filter( 'emoji_svg_url', '__return_empty_string' );

/**
 * Отключение смайлов emoji
 */
function disable_emojis() {

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 *
 * @return array Difference between the two arrays
 */
function disable_emojis_tinymce( $plugins ) {

	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Отключение встроенной функции восстановления пароля пользователя
 */
add_action( 'lost_password', 'lost_password_disable' );
function lost_password_disable() {

	wp_die( 'Функция восстановления пароля временно заблокирована, обратитесь, пожалуйта, к <a href="/contacts/">администратору</a>.', 'Восстановление пароля заблокировано', [ 'response' => 404 ] );
}

/**
 * Отключение xmlrpc
 * Отключение вывода ссылок в шаблон: надо узнать
 */
add_filter( 'xmlrpc_enabled', '__return_false' );

/**
 * Отключение вывода: <meta name="generator" content="WordPress 4.9.5" />
 */
add_filter( 'the_generator', '__return_empty_string' );

/**
 * Отключение вывода: <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://site.com/wp-includes/wlwmanifest.xml" />
 * Нужна для работы софта вроде "Windows Live Writer"
 */
remove_action( 'wp_head', 'wlwmanifest_link' );

/**
 * Отключение вывода: <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://site.com/xmlrpc.php?rsd" />
 * Возможность редактирования внешними сервисами
 */
remove_action( 'wp_head', 'rsd_link' );

/**
 * Отключение вывода: <link rel="shortlink" ...>
 * 1) из кода генерируемой страницы сайта
 * 2) из заголовков ответа сервера
 */
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
remove_action( 'template_redirect', 'wp_shortlink_header', 11 );

/**
 * Отключение вывода: <link rel="next" ...> & prev
 */
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );

/**
 * Отключение подключения css-файла во фронтенд части "/wp-includes/css/dist/block-library/style.min.css"
 * Файл отвечает за оформление Gutenberg и не нужен, если используется Classic Editor
 */
function remove_gutenberg_block_library_style_css() {
	wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'remove_gutenberg_block_library_style_css' );