<?php
/**
 * @param WP_Query|null $wp_query
 * @param bool $echo
 *
 * @return string
 * Accepts a WP_Query instance to build pagination (for custom wp_query()),
 * or nothing to use the current global $wp_query (eg: taxonomy term page)
 * - Tested on WP 4.9.5
 * - Tested with Bootstrap 4.1
 * - Tested on Sage 9
 *
 * USAGE:
 *     <?php echo bootstrap_pagination(); ?> //uses global $wp_query
 * or with custom WP_Query():
 *     <?php
 *      $query = new \WP_Query($args);
 *       ... while(have_posts()), $query->posts stuff ...
 *       echo bootstrap_pagination($query);
 *     ?>
 */
function bootstrap_pagination( \WP_Query $wp_query = null, $echo = true ) {

	if ( null === $wp_query ) {
		global $wp_query;
	}

	$pages = paginate_links( [
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'format'       => '?paged=%#%',
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'total'        => $wp_query->max_num_pages,
			'type'         => 'array',
			'show_all'     => false,
			'end_size'     => 3,
			'mid_size'     => 1,
			'prev_next'    => false,
			'prev_text'    => __( '« Prev' ),
			'next_text'    => __( 'Next »' ),
			'add_args'     => false,
			'add_fragment' => ''
		]
	);

	if ( is_array( $pages ) ) {
		$pagination = '<ul class="pagination justify-content-center">';

		foreach ( $pages as $page ) {
			$pagination .= '<li class="page-item' . ( strpos( $page, 'current' ) !== false ? ' active' : '' ) . '"> ' . str_replace( 'page-numbers', 'page-link', $page ) . '</li>';
		}
		$pagination .= '</ul>';

		if ( $echo ) {
			echo $pagination;
		}
		else {
			return $pagination;
		}
	}

	return null;
}

/**
 * Автоматическое добавление атрибута id в заголовки H2
 * через транслитерацию их содержимого
 */
function add_auto_id_attr_in_h2_headers( $content ) {

	if ( is_singular( 'post' ) ) {
		return preg_replace_callback(
			'!<(h2|h3)>(.*?)</(h2|h3)>!',
			function ( $matches ) {
				return '<' . $matches[1] . ' id="' . transliteration( $matches[2] ) . '">' . $matches[2] . '</' . $matches[1] . '>';
			},
			$content
		);
	}

	return $content;
}
add_filter( 'the_content', 'add_auto_id_attr_in_h2_headers' );

/**
 * Автоматическое добавление класса тега <p>,
 * внутри которого есть картинка, чтобы сделать margin-x: -15px
 * в мобильной версии сайта
 *
 * @param $content
 *
 * @return string
 */
function add_auto_class_for_p_img_block( $content ) {

	if ( is_single() OR is_page() ) {
		$content = str_replace( '<p><img', '<p class="img-box"><img', $content );
	}

	return $content;
}
add_filter( 'the_content', 'add_auto_class_for_p_img_block' );

/**
 * 1) Добавление указателя (MDI) внешней ссылки в контенте страницы|поста
 * 2) Добавление атрибута nofollow
 *
 * @param $content string Исходный текст статьи
 *
 * @return string
 */
function add_icon_for_external_links_in_content( $content ) {

	if ( is_single() ) {
		// проверяем, содержит ли контент внешние ссылки
		// и только если они найдены, делаем замену
		if ( false !== strpos( $content, '<a href="http' ) ) {

			// если в админке ссылка была указана как _blank, то добавились ненужные атрибуты (rel, target) - убираем их
			$content = str_replace( [ ' rel="noopener noreferrer"', ' target="_blank"' ], '', $content );

			$content = preg_replace_callback(

				'!<a href="(https?:.*?)">(.*?)</a>!',

				function ( $matches ) {

					if ( ! empty( $matches[1] ) ) {

						if(strpos($matches[2], '<i') === false)
							$url = '<a class="ext" title="Внешняя ссылка" href="' . $matches[1] . '" target="_blank" rel="nofollow noopener noreferrer">' . $matches[2] . '<i class="material-icons">link</i></a>';
						else
							$url = '<a class="ext" href="' . $matches[1] . '" target="_blank" rel="nofollow noopener noreferrer">' . $matches[2] . '</a>';

						return $url;
					}

					return $matches[0];
				},

				$content
			);
		}

		// если ссылка содержит адрес настроек браузера
		if ( false !== strpos( $content, '<a href="chrome://' ) ) {

			$content = preg_replace_callback(

				'!<a href="(chrome:.*?)">(.*?)</a>!',

				function ( $matches ) {

					if ( ! empty( $matches[1] ) ) {

						$matches[2] = str_ireplace( [ '&#171;', '&#187;' ], '', $matches[2] );
						$url = '<a class="int" href="' .$matches[1]. '" target="_blank">' . $matches[2] . '<i class="material-icons">settings</i>' . '</a>';

						return $url;
					}

					return $matches[0];
				},

				$content
			);
		}
	}

	return $content;
}
add_filter( 'the_content', 'add_icon_for_external_links_in_content' );

/**
 * Автоматическое формирование блока Bootstrap Tabs для H3 заголовков "Windows" и "Android"
 *
 * @param $content
 *
 * @return string
 */
function bootstrap_tabs_convert_h3_blocks ( $content ) {

	if ( stripos( $content, '<h3 id="windows">windows</h3>' ) !== false ) {

		$counter = 0;

		$content = preg_replace_callback(
			'@<h3 id="windows">Windows</h3>([\s\S]*?)(<h2|<h3(?! id="android")|<p class="alert|\Z)@mi',
			function ( $matches ) use ( &$counter ) {

				$counter++;

				$h3_block = explode( '<h3 id="android">Android</h3>', $matches[1] );

				return <<<HTML
<nav class="os-tabs">
	<div id="nav-tab" class="nav nav-tabs" role="tablist">
		<a id="nav-windows-tab" class="nav-item nav-link active" role="tab" href="#nav-windows-$counter" data-toggle="tab" aria-controls="nav-windows" aria-selected="true">
			<i class="material-icons">desktop_windows</i><b>Windows</b>
		</a>
		<a id="nav-android-tab" class="nav-item nav-link" role="tab" href="#nav-android-$counter" data-toggle="tab" aria-controls="nav-android" aria-selected="false">
			<i class="material-icons">android</i><b>Android</b>
		</a>
	</div>
</nav>
<div class="tab-content os-tabs-content pt-3">
	<div id="nav-windows-$counter" class="tab-pane fade show active" role="tabpanel" aria-labelledby="nav-windows-tab">
		{$h3_block[0]}
	</div>
	<div id="nav-android-$counter" class="tab-pane fade" role="tabpanel" aria-labelledby="nav-android-tab">
		{$h3_block[1]}	
	</div>
</div>
$matches[2]
HTML;

			},
			$content
		);
	}

	return $content;
}
add_filter( 'the_content', 'bootstrap_tabs_convert_h3_blocks' );