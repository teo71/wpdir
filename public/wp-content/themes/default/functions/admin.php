<?php
/**
 * Отправка уведомлений о новых комментариях только админу сайта
 */
function comment_notification_only_to_admin( $emails, $comment_id ) {
	$admin_email = get_bloginfo( 'admin_email' );

	return [ $admin_email ];
}
add_filter( 'comment_notification_recipients', 'comment_notification_only_to_admin', 10, 2 );

/**
 * Формирование "чистого" блока img без лишнего мусора при вставке изображения в TinyMCE
 */
function wpext_image_send_to_editor( $html, $id, $caption, $title, $align, $url, $size, $alt ) {

	// get img thumb 150x150
	$get_img_thumb = image_downsize( $id, $size );

	// if thumb and img url exists
	if ( false !== $get_img_thumb AND ! $url ) {

		// abs src link to relative
		$get_img_thumb[0] = esc_url( wp_make_link_relative( $get_img_thumb['0'] ) );

		$alt = esc_attr( $alt );

		$html = '<img src="' . $get_img_thumb[0] . '" alt="' . $alt . '">';
	}

	return $html;
}
add_filter( 'image_send_to_editor', 'wpext_image_send_to_editor', 1, 8 );

/**
 * Добавление тегов в белый список TinyMCE
 *
 * @param $initArray array
 *
 * @return array
 */
function override_tiny_mce_options( $initArray ) {
	$initArray['extended_valid_elements'] = 'span[*]';

	return $initArray;
}
add_filter( 'tiny_mce_before_init', 'override_tiny_mce_options' );

/**
 * Добавление css в TinyMCE:
 * 1. подключение MDI
 * 2. подключение части Bootstrap оформления в редатктор
 */
function my_theme_add_editor_styles() {

	$ver = ( WP_DEBUG ) ? '?v=' . time() : '';

	add_editor_style( '//fonts.googleapis.com/icon?family=Material+Icons' );
	add_editor_style( 'css/editor-admin.min.css' . $ver );
}
add_action( 'after_setup_theme', 'my_theme_add_editor_styles' );

/**
 * Преобразование абсолютных URL текущего домена в относительные при сохранении записи
 *
 * @param $content string Исходный текст статьи
 *
 * @return string Текст статьи без абсолютных ссылок текущего домена
 */
function content_save_pre_absolute_links_to_relative( $content ) {

	$current_domain = str_replace( [ 'http://', 'https://' ], '', home_url() );

	$content = preg_replace_callback(

		'|href=\\\\"https?://' . $current_domain . '(/?.*)\\\\"|iU',
		function ( $matches ) {
			
			if ( isset( $matches[0] ) ) {
				return 'href="' . str_replace( '//', '/', '/' . $matches[1] ) . '"';
			}

			return $matches[0];
		},

		$content
	);

	return $content;
}
add_filter( 'content_save_pre', 'content_save_pre_absolute_links_to_relative' );

/**
 * Добавление атрибута target="_blank" к ссылка просмотра новости из их списка в админпанели
 *
 * @param $actions array
 *
 * @return array
 */
function post_row_actions_view_blank( $actions ) {

	$actions['view'] = str_replace( '<a href', '<a target="_blank" href', $actions['view'] );

	return $actions;
}
add_filter( 'post_row_actions', 'post_row_actions_view_blank', 10, 1 );

/**
 * Добавление новой колонки в список постов с кол-вом знаков поста
 *
 * @param $post_columns array
 *
 * @return array
 */
function post_column_admin_letter_count( $post_columns ) {

	$new_columns['letter_count'] = 'Кол-во знаков';

	return array_slice( $post_columns, 0, 2 ) + $new_columns + array_slice( $post_columns, 2 );
}
add_filter( 'manage_posts_columns', 'post_column_admin_letter_count', 10, 1 );

/**
 * Подсчёт кол-ва знаков без пробелов для каждого поста
 *
 * @param $column_name string
 */
function post_column_admin_letter_count_content( $column_name ) {

	if ( $column_name === 'letter_count' ) {

		$post = get_post();

		$letter_count = strip_tags( stripslashes( $post->post_content ) );
		echo mb_strlen( str_replace( ' ', '', $letter_count ) ) . ' / ' . mb_strlen( $letter_count );
	}
}
add_action( 'manage_post_posts_custom_column', 'post_column_admin_letter_count_content' );