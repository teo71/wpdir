<?php

/**
 * Транслитерация символов RU>EN
 *
 * @param $str string
 *
 * @return string
 */
function transliteration( $str ) {
	$trans = [
		"а" => "a",
		"б" => "b",
		"в" => "v",
		"г" => "g",
		"д" => "d",
		"е" => "e",
		"ё" => "yo",
		"ж" => "j",
		"з" => "z",
		"и" => "i",
		"й" => "i",
		"к" => "k",
		"л" => "l",
		"м" => "m",
		"н" => "n",
		"о" => "o",
		"п" => "p",
		"р" => "r",
		"с" => "s",
		"т" => "t",
		"у" => "y",
		"ф" => "f",
		"х" => "h",
		"ц" => "c",
		"ч" => "ch",
		"ш" => "sh",
		"щ" => "shh",
		"ы" => "i",
		"э" => "e",
		"ю" => "u",
		"я" => "ya",
		"ї" => "i",
		"'" => "",
		"ь" => "",
		"Ь" => "",
		"ъ" => "",
		"Ъ" => "",
		"і" => "i",
		"А" => "A",
		"Б" => "B",
		"В" => "V",
		"Г" => "G",
		"Д" => "D",
		"Е" => "E",
		"Ё" => "Yo",
		"Ж" => "J",
		"З" => "Z",
		"И" => "I",
		"Й" => "I",
		"К" => "K",
		"Л" => "L",
		"М" => "M",
		"Н" => "N",
		"О" => "O",
		"П" => "P",
		"Р" => "R",
		"С" => "S",
		"Т" => "T",
		"У" => "Y",
		"Ф" => "F",
		"Х" => "H",
		"Ц" => "C",
		"Ч" => "Ch",
		"Ш" => "Sh",
		"Щ" => "Sh",
		"Ы" => "I",
		"Э" => "E",
		"Ю" => "U",
		"Я" => "Ya",
		"Ї" => "I",
		"І" => "I"
	];
	$res   = str_replace( " ", "-", strtr( mb_strtolower( $str, 'utf-8' ), $trans ) );
	//если надо, вырезаем все кроме латинских букв, цифр и дефиса (например для формирования логина)
	$res = preg_replace( "|[^a-zA-Z0-9-]|", "", $res );

	return $res;
}

/**
 * Получение полей текущего поста/страницы/категории и т.п. ACF плагина
 * https://www.advancedcustomfields.com/resources/get_fields/
 *
 * @param $object_id string
 *
 * @return array|bool
 */
function get_current_post_ACF_fields( $object_id = '' ) {

	if ( is_single() OR is_page() OR is_category() ) {
		return ( ! empty( $object_id ) ) ? get_fields( $object_id ) : get_fields( );
	}

	return false;
}

/**
 * Функция записи в стандартный логфайл debug.log, если отладка включена
 * @param $str string
 * @return void
 */
function write_log( $str ) {
	if ( true === WP_DEBUG AND true === WP_DEBUG_LOG AND ! empty( $str ) ) {
		error_log( $str );
	}
}

/**
 * Создание блока "Содержание" на основе H2/H3 заголовков страницы
 * @param $content string
 * @return string|array
 */
function toc_generator( $content ) {

	$toc = [];
	if ( preg_match_all( '#<(h2|h3)>(?!(windows|android))(.*?)</(h2|h3)>#i', $content, $headers ) ) {

		$h3_list = [];
		foreach ( $headers[1] as $key => $value ) {

			if ( $value === 'h2' ) {

				$h3_list_html = '';
				if ( ! empty( $h3_list ) ) {
					$h3_list_html = '<ul>' . implode( '', $h3_list ) . '</ul>';
					// обнуление внутреннего буфера H3 списка
					$h3_list = [];
				}
				$toc[] = $h3_list_html . '<li><a href="#' . transliteration( $headers[3][ $key ] ). '">' . $headers[3][ $key ] . '</a></li>';
			} else {
				$h3_list[] = '<li><a href="#' . transliteration( $headers[3][ $key ] ). '">' . $headers[3][ $key ] . '</a></li>';
			}
		}

		if ( ! empty( $toc ) ) {
			$toc = '<ol>' . implode( '', $toc ) . '</ol>';

			if ( ! empty( $h3_list ) ) {
				$h3_list_html = '<ul>' . implode( '', $h3_list ) . '</ul>';
				$toc = str_replace( '</li></ol>', $h3_list_html . '</li></ol>', $toc );
			}

			$toc = str_replace( '</li><ul>', '<ul>', $toc );
			$toc = str_replace( '</ul><li>', '</ul></li><li>', $toc );
		}

		// Добавление пункта "Начало"
		if ( ! empty( $toc ) ) {
			$toc = str_replace( '<ol>', '<ol><li><a href="#start">Начало</a></li>', $toc );
		}
	}

	return $toc;
}

/**
 * Генерация ссылки на нужный тип дистрибутива Chrome
 *
 * @param $chromeURLParts
 * @param $branch
 * @param $bit
 * @param string $type
 *
 * @return array|string
 */
function buildChromeDownloadURL( $chromeURLParts, $branch, $bit ) {

	// результат в виде списка ссылок
	$chromeDownloadLinks = [];

	// получение частей URL нужной ветки/битности/типа
	$url_parts = [];

	// offline тип дистрибутива
	if ( $branch !== 'canary' ) {
		$url_parts['offline'] = array_merge( $chromeURLParts['main'], $chromeURLParts[ $branch ][ $bit ]['offline'] );
	}

	// online тип дистрибутива
	$url_parts['online'] = array_merge( $chromeURLParts['main'], $chromeURLParts[ $branch ][ $bit ]['online'] );

	// web [offline] тип дистрибутива
	if ( $branch !== 'canary' ) {
		$url_parts['web']['offline'] = $chromeURLParts[ $branch ][ $bit ]['web']['offline'];
	}

	// web [online] тип дистрибутива
	$url_parts['web']['online'] = $chromeURLParts[ $branch ][ $bit ]['web']['online'];

	// offline
	if ( $branch !== 'canary' ) {
		$buildURL = [];
		foreach ( $url_parts['offline'] as $item ) {
			$item = $item[0] . urlencode( '=' ) . $item[1];
			$item = str_replace( '%20', '%2520', $item );
			$item = str_replace( '{', '%7B', $item );
			$item = str_replace( '}', '%7D', $item );

			$buildURL[] = $item;
		}
		$buildURL = implode( urlencode( '&' ), $buildURL );

		$chromeDownloadLinks['offline'] = '<a class="dropdown-item" href="' . $buildURL . '" target="_blank">оффлайн (~60 МБ)</a>';
	}

	// online
	$buildURL = [];
	foreach ( $url_parts['online'] as $item ) {
		$item = $item[0] . urlencode( '=' ) . $item[1];
		$item = str_replace( '%20', '%2520', $item );
		$item = str_replace( '{', '%7B', $item );
		$item = str_replace( '}', '%7D', $item );

		$buildURL[] = $item;
	}
	$buildURL = implode( urlencode( '&' ), $buildURL );

	$chromeDownloadLinks['online'] = '<a class="dropdown-item" href="' . $buildURL . '" target="_blank">онлайн (1-2 МБ)</a>';

	// web
	if ( $branch !== 'canary' ) {
		$chromeDownloadLinks['divider'] = '<div class="dropdown-divider"></div>';
		$chromeDownloadLinks['web_offline'] = '<a class="dropdown-item" href="' . $url_parts['web']['offline'] . '" target="_blank">оффлайн (оф. сайт)</a>';
	}
	$chromeDownloadLinks['web_online'] = '<a class="dropdown-item" href="' . $url_parts['web']['online'] . '" target="_blank">онлайн (оф. сайт)</a>';

	// result (html)
	$chromeDownloadLinks = implode( "\n", $chromeDownloadLinks );
	$chromeDownloadLinks = str_replace( '">', '" rel="nofollow noopener noreferrer">', $chromeDownloadLinks );

	return $chromeDownloadLinks;
}

/*
 * Части URL для генеарции 4-х типов ссылок для 4-х веток
 * дистрибутивов Google Chrome
 */
$chromeURLParts = [
	'main' => [
		[ 'https://dl.google.com/tag/s/appguid', '{8A69D345-D564-463C-AFF1-A69D9E530F96}' ],
		[ 'iid', '{77777777-7777-7777-7777-777777777777}' ],
		[ 'lang', 'ru' ],
		[ 'browser', '0' ],
		[ 'usagestats', '0' ],
	],
	'stable' => [
		'64' => [
			'offline' => [
				[ 'appname', 'Google%20Chrome' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', 'x64-stable/update2/installers/ChromeStandaloneSetup64.exe' ]
			],
			'online' => [
				[ 'appname', 'Google%20Chrome' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', 'x64-stable/update2/installers/ChromeSetup.exe' ]
			],
			'web' => [
				'offline' => 'http://www.google.com/chrome/?platform=win64&standalone=1',
				'online' => 'https://www.google.com/chrome/?platform=win64'
			]
		],
		'32' => [
			'offline' => [
				[ 'appname', 'Google%20Chrome' ],
				[ 'needsadmin', 'prefers/update2/installers/ChromeStandaloneSetup.exe' ],
			],
			'online' => [
				[ 'appname', 'Google%20Chrome' ],
				[ 'needsadmin', 'prefers/update2/installers/ChromeSetup.exe' ],
			],
			'web' => [
				'offline' => 'http://www.google.com/chrome/?platform=win&standalone=1',
				'online' => 'https://www.google.com/chrome/?platform=win'
			]
		]
	],
	'beta' => [
		'64' => [
			'offline' => [
				[ 'appname', 'Google%20Chrome%20Beta' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', '-arch_x64-statsdef_1' ],
				[ 'installdataindex', 'empty/chrome/install/beta/ChromeBetaStandaloneSetup64.exe' ],
			],
			'online' => [
				[ 'appname', 'Google%20Chrome%20Beta' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', 'x64-beta/update2/installers/ChromeSetup.exe' ],
			],
			'web' => [
				'offline' => 'http://www.google.com/chrome/beta/?platform=win64&standalone=1',
				'online' => 'https://www.google.com/chrome/beta/?platform=win64'
			]

		],
		'32' => [
			'offline' => [
				[ 'appname', 'Google%20Chrome%20Beta' ],
				[ 'needsadmin', 'prefers' ],
				[ 'installdataindex', 'empty/chrome/install/beta/ChromeBetaStandaloneSetup.exe' ],
			],
			'online' => [
				[ 'appname', 'Google%20Chrome%20Beta' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', '1.1-beta/update2/installers/ChromeSetup.exe' ],
			],
			'web' => [
				'offline' => 'http://www.google.com/chrome/beta/?platform=win&standalone=1',
				'online' => 'https://www.google.com/chrome/beta/?platform=win'
			]
		]
	],
	'developer' => [
		'64' => [
			'offline' => [
				[ 'appname', 'Google%20Chrome%20Dev' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', '-arch_x64-statsdef_1' ],
				[ 'installdataindex', 'empty/chrome/install/dev/ChromeDevStandaloneSetup64.exe' ],
			],
			'online' => [
				[ 'appname', 'Google%20Chrome%20Dev' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', 'x64-dev/update2/installers/ChromeSetup.exe' ],
			],
			'web' => [
				'offline' => 'http://www.google.com/chrome/dev/?platform=win64&standalone=1',
				'online' => 'https://www.google.com/chrome/dev/?platform=win64'
			]
		],
		'32' => [
			'offline' => [
				[ 'appname', 'Google%20Chrome%20Dev' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', '-arch_x86-statsdef_1' ],
				[ 'installdataindex', 'empty/chrome/install/dev/ChromeDevStandaloneSetup.exe' ],
			],
			'online' => [
				[ 'appname', 'Google%20Chrome%20Dev' ],
				[ 'needsadmin', 'prefers' ],
				[ 'ap', '2.0-dev/update2/installers/ChromeSetup.exe' ],
			],
			'web' => [
				'offline' => 'http://www.google.com/chrome/dev/?platform=win&standalone=1',
				'online' => 'https://www.google.com/chrome/dev/?platform=win'
			]
		]
	],
	'canary' => [
		'64' => [
			'offline' => [],
			'online' => [
				[ 'appname', 'Google%20Chrome%20Canary' ],
				[ 'needsadmin', 'false' ],
				[ 'ap', 'x64-canary/update2/installers/ChromeSetup.exe' ],
			],
			'web' => [
				'offline' => [],
				'online' => 'https://www.google.com/chrome/canary/?platform=win64'
			]
		],
		'32' => [
			'offline' => [],
			'online' => [
				[ 'appname', 'Google%20Chrome%20Canary' ],
				[ 'needsadmin', 'false/update2/installers/ChromeSetup.exe' ],
			],
			'web' => [
				'offline' => [],
				'online' => 'https://www.google.com/chrome/canary/?platform=win'
			]
		]
	]
];
/*
 * Добавление массива в кеш для возможности
 * использования в файлах шаблона
 */
wp_cache_set( 'chromeURLParts', $chromeURLParts );

/**
 * Вывод N записей в блок "Рекомендуем" на главной странице сайта
 * Записи добавляются в список через соответствующее поле ACF плагина
 *
 * @param string $type (recommended|interesting) тип блока
 * @param int $number количество записей по умолчанию
 *
 * @return string
 */
function get_main_sidebar_posts( $type, $number = 5 ) {

	$type = in_array( $type, [ 'recommended', 'interesting' ] ) ? $type : 'recommended';
	$number = intval( $number ) ? intval( $number ) : 5;
	$sidebar_posts = [];

	$posts = get_posts( [
		'numberposts' => $number,
		'post_type'   => 'post',
		'meta_key'    => $type,
		'meta_value'  => '1'
	] );

	foreach ( $posts as $item ) {
		$sidebar_posts[] = '<li><a href="/' . $item->post_name . '/">' . $item->post_title . '</a></li>';
	}

	return ( ! empty( $sidebar_posts ) ) ? '<ul>' . implode( '', $sidebar_posts ) . '</ul>' : '';
}