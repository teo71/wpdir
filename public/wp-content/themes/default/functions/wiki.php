<?php
/**
 * Получения правильного окончания слова "статья"
 * в зависимости от кол-ва (цифры) статей в категории
 *
 * @param $number int
 *
 * @return string
 */
function set_word_ending_depending_number( $number ) {

	// варианты окончаний слова "статья"
	$ending = [
		0 => 'ей',
		1 => 'ья',
		2 => 'ьи',
		3 => 'ьи',
		4 => 'ьи',
		5 => 'ей',
		6 => 'ей',
		7 => 'ей',
		8 => 'ей',
		9 => 'ей',
	];

	$last_digit = substr( intval( $number ), - 1 );
	if ( false !== $last_digit AND '' != $last_digit ) {
		return $number . ' стат' . $ending[ $last_digit ];
	}

	return '';
}