<h1><?php the_title(); ?></h1>
<?php the_content(); ?>

<?php $categories = get_categories( [ 'hide_empty' => false ] ); ?>
<?php if ( ! empty( $categories ) ): ?>
<div class="row">
    <?php foreach ( $categories as $cat_item ): ?>
        <div class="col-12 col-md-6 col-xxl-4">

            <div class="card">
                <div class="card-header">
                    <a href="<?php echo get_category_link( $cat_item->term_id ); ?>"><?php echo $cat_item->name; ?></a>
                </div>
                <div class="card-body">

                    <?php
                    // Получение 3 последних постов из категории
                    $get_cat_posts = get_posts( [
	                    'numberposts' => 3,
	                    'category'    => $cat_item->cat_ID,
                    ] );
                    ?>
                    <?php if ( ! empty( $get_cat_posts ) ): ?>
                    <ul>
	                    <?php foreach ( $get_cat_posts as $cat_post ): ?>
                        <li>
                            <a href="<?php the_permalink( $cat_post->ID ); ?>"><?php echo $cat_post->post_title; ?></a>
                        </li>
                        <?php endforeach; ?>
                        <li class="divider">
                            ...
                        </li>
                        <li>
                            <a href="<?php echo get_category_link( $cat_item->term_id ); ?>">ещё <?php echo set_word_ending_depending_number($cat_item->category_count - 3); ?></a>
                        </li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>


