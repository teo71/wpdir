<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Советник Google Chrome</title>
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
<style>
    body {
        background-color: #eeeeee;
    }
    h1 {
        display: block;
        margin: 3rem 0 2rem 0;
        font-size: 2rem;
        text-align: center;
    }
    span {
        display: block;
        width: 50%;
        margin: 0 auto;
        text-align: center;
    }
</style>
<h1>Сайт находится в стадии разработки</h1>
<span>Проект целиком посвящён браузеру Google Chrome - раздел материалов с описанием функций программы, советы по настройке, ссылки для скачивания дистрибутивов всех доступных версий.</span>
</body>
</html>