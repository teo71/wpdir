<h1><?php the_title(); ?></h1>
<?php the_content(); ?>


<?php
// получение ссылок текущей страницы
$dl_links = get_fields(22);
?>
<div class="card">
    <div class="card-body p-0">
        <table class="table mb-0">
            <tbody>
                <tr>
                    <td class="border-0">
                        <div class="text-success">Стабильная</div>
                        <div class="d-none d-sm-block">Рекомендуемая к установке для большинства пользователей версия</div>
                    </td>
                    <td class="border-0">
                        <a target="_blank" href="<?php echo $dl_links['android_stable']; ?>" class="btn btn-outline-secondary" rel="nofollow noopener noreferrer">
                            <i class="material-icons">get_app</i>Скачать
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-warning">Beta</div>
                        <div class="d-none d-sm-block">Если желаете проверить функции, которые скоро появятся в стабильной ветке</div>
                    </td>
                    <td>
                        <a target="_blank" href="<?php echo $dl_links['android_beta']; ?>" class="btn btn-outline-secondary" rel="nofollow noopener noreferrer">
                            <i class="material-icons">get_app</i>Скачать
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-danger">Developer</div>
                        <div class="d-none d-sm-block">Возможность теста самых свежих идей и, разумеется, багов будущих версий приложения</div>
                    </td>
                    <td>
                        <a target="_blank" href="<?php echo $dl_links['android_developer']; ?>" class="btn btn-outline-secondary" rel="nofollow noopener noreferrer">
                            <i class="material-icons">get_app</i>Скачать
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>Canary</div>
                        <div class="d-none d-sm-block">Ещё более ранние изменения, чем в Developer. Может быть установлена параллельно с любой другой, имеет собственный канал обновлений.</div>
                    </td>
                    <td>
                        <a target="_blank" href="<?php echo $dl_links['android_canary']; ?>" class="btn btn-outline-secondary" rel="nofollow noopener noreferrer">
                            <i class="material-icons">get_app</i>Скачать
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
