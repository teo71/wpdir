<?php
/**
 * Форма добавления комментария
 *
 */
$commenter    = wp_get_current_commenter();
$req          = get_option( 'require_name_email' );
$html_req     = ( $req ? " required='required'" : '' );
$comment_args = [

	'fields' => [

		'author' => '<div class="form-group">
                        <input class="form-control form-control-sm" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" maxlength="245" placeholder="Имя"' . $html_req . ' />
                    </div>',
	],

	'comment_field'        => '<div class="form-group"><textarea class="form-control form-control-sm" id="comment" name="comment" maxlength="65525" placeholder="Текст комментария будет одобрен после модерации" required="required"></textarea></div>',
	'logged_in_as'         => '',
	'comment_notes_before' => '',
	'class_form'           => 'card-body',
	'title_reply'          => 'Оставить комментарий',
//	'title_reply_to'       => 'Ответить',
	'title_reply_before'   => '<div class="card mt-4"><div id="comments" class="comm-addform-title card-header">',
	'title_reply_after'    => '</div>',
	'cancel_reply_before'  => ' <small>',
	'cancel_reply_after'   => '</small>',
	'cancel_reply_link'    => '<button type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>',
	'label_submit'         => 'Отправить',
	'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="btn btn-darkblue btn-sm" value="%4$s" />',
	'submit_field'         => '<div class="">%1$s %2$s</div>',
	'format'               => 'html5'
];
//<p class="alert alert-secondary comment-form-alert">Комментарии публикуются только после ручной модерации. Обратите, пожалуйста, внимание.</p>
?>

<?php comment_form( $comment_args ); ?>
</div>

<?php if ( have_comments() ): ?>

    <div class="comments-number">
        <?php comments_number(); ?>:
    </div>
	<div class="comment-list">
		<?php
		wp_list_comments( [

			'style'       => 'div',
			'avatar_size' => 40,
			'callback'    => 'custom_comment'
		] );
		?>
	</div>

<?php endif; ?>

<?php
function custom_comment( $comment, $args, $depth ) {

    $args['depth']      = $depth;
    $args['reply_text'] = '<i class="material-icons">reply</i>';
//    $args['reply_text'] = '<button class="btn btn-sm btn-light">ответить</button>';
    $args['add_below']  = 'reply-box';
?>

    <div id="comment-<?php comment_ID(); ?>" <?php comment_class('comment-item'); ?>>
        <div id="reply-box-<?php comment_ID(); ?>" class="reply-box row">
            <div class="col-auto d-none d-md-block">
                <div class="auth">
                    <?php
                    if ( $args['avatar_size'] != 0 ) {
	                    echo get_avatar( $comment, $args['avatar_size'], '', '', ['class' => ( $depth >= 2 ) ? 'avatar-depth' : ''] );
                    }
                    ?>
                </div>
            </div>
            <div class="col">

                <div class="comm-body">
                    <div class="comm-header">
	                    <span class="comm-author<?php echo ('YaUser' == get_comment_author()) ? ' comm-author-red' : ''; ?>">
                            <?php comment_author(); ?>
                        </span>
                        <span class="comm-date">
                            <a href="<?php echo get_comment_link(); ?>"><?php comment_date( 'd-m-Y H:i' ); ?></a>
                        </span>
                        <span class="reply-link">
                            <?php comment_reply_link($args); ?>
                        </span>
                    </div>
                    <div class="comm-content">
	                    <?php comment_text(); ?>
                    </div>
                </div>

            </div>
        </div>

<?php
}
?>
