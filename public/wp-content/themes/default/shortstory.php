<div class="col-12 col-md-6 shortstory">
    <div class="short-body">
        <div>
            <img src="<?php the_field( 'picture_skene', $post->ID ); ?>" alt="Обложка скина <?php echo esc_attr( get_the_title() ); ?>">
        </div>
        <div class="row">
            <div class="col"><?php the_title(); ?></div>
            <div class="col-auto">
                <a class="btn btn-secondary btn-sm" target="_blank" rel="nofollow" href="<?php the_field( 'link_jump_skin', $post->ID ); ?>">
                    <span class="oi oi-arrow-bottom"></span> Скачать
                </a>
            </div>
        </div>
    </div>
</div>