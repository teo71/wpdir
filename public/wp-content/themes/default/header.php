<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head(); ?>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

        <div class="menu-toggle d-lg-none">
            <button class="btn btn-salad btn-sm">
                <i class="material-icons">menu</i>
            </button>
        </div>

        <a title="Помощник Google Chrome" class="navbar-brand flex-grow-1 flex-lg-grow-0" href="/">
            Chrome<span>Advisor</span>
        </a>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/help/">Помощь</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/windows/">Windows</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/android/">Android</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/portable/">Portable</a>
                </li>
                <li class="nav-item">
                    <a title="Скачать Google Chrome" class="btn btn-darkgreen btn-sm" href="/download/">
                        <i class="material-icons">get_app</i>Скачать
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about/">О проекте</a>
                </li>
            </ul>
            <button type="button" class="close d-lg-none" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="search-toggle d-lg-none">
            <button class="btn btn-salad btn-sm">
                <i class="material-icons">search</i>
            </button>
        </div>

	    <?php
	    // иконка установки ширины контейнера контента
	    // в зависмости от cookie 'fullscreen' (0 - xxl, undefined - fluid)
	    $fullscreen_icon = ( ! isset( $_COOKIE['fullscreen'] ) ) ? 'fullscreen_exit' : 'fullscreen';
	    ?>
        <a class="fullscreen d-none d-xl-block" title="Изменить ширину сайта" href="#"><i class="material-icons"><?php echo $fullscreen_icon; ?></i></a>

        <form method="get" action="/" id="search-form" class="form-inline">
            <input name="s" class="form-control" type="search" placeholder="Поиск" aria-label="Search">
            <div class="search-button">
                <i class="material-icons">search</i>
            </div>
        </form>

    </nav>
</header>