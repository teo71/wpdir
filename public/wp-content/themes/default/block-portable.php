<h1><?php echo set_document_custom_h1_content( get_the_title() ); ?></h1>
<?php the_content(); ?>

<?php
// получение ссылок текущей странциы (Android дистрибутивов)
$dl_links = get_fields(22);
?>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tbody>
                <tr>
                    <td>
                        <div>Cтабильная</div>
                        <div>последняя стабильная версия Google Chrome Portable</div>
                    </td>
                    <td>
                        <a href="<?php echo $dl_links['portable']; ?>" target="_blank" class="btn btn-outline-secondary" rel="nofollow noreferrer noopener">
                            <i class="material-icons">get_app</i>Скачать
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>Старые версий</div>
                        <div>архив портативных версий c 2009 года, включая ветки stable, beta и developer</div>
                    </td>
                    <td>
                        <a href="<?php echo $dl_links['portable_archive']; ?>" target="_blank" class="btn btn-outline-secondary" rel="nofollow noreferrer noopener">
                            <i class="material-icons">get_app</i>Скачать
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
