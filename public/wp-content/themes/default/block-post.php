<?php
// В зависимости от значения cookie "sidebar_right"
// скрываем или показываем sidebar при помощи класса
$sb_none = ( isset( $_COOKIE['sidebar_right'] ) ) ? ' sb-none' : '';

// Формирование блока "Содержание" через парсинг заголовков контента статьи
$toc_content = toc_generator( get_the_content() );

// Вывод блока "Топ статьи", отмеченные через ACF вручную
$category_top_posts = get_posts( [
	'numberposts' => 4,
	'category'    => $post->post_category[0],
	'meta_key'    => 'top_post',
	'meta_value'  => 1,
	'exclude'     => $post->ID
] );
?>
<div class="row post-header">
    <div class="col-12 col-lg">
        <h1><?php echo set_document_custom_h1_content( get_the_title() ); ?></h1>
		<?php if ( current_user_can( 'administrator' ) ): ?>
            <a title="Редактировать запись" class="edit_link" target="_blank" href="<?php echo get_edit_post_link(); ?>"><i class="material-icons">edit</i></a>
		<?php endif; ?>
    </div>
    <div class="col-12 col-lg-auto">
        <div class="bread-crumbs mb-3 mb-lg-0">
            <a href="/" title="Помощник Google Chrome"><i class="material-icons">web_asset</i></a> / <a href="/help/">Помощь</a> / <?php the_category(' <span>|</span> '); ?>
        </div>
    </div>
</div>
<div class="card card-post">
    <div class="card-header">
        <div class="row">
            <div class="col post-date">
				<?php
				/* даты добавления и редактирования статьи */
				$post_add_date  = get_the_date();
				$post_edit_date = the_modified_date('d.m.Y', '', '', false);
				?>
                <span title="Дата публикации"><i class="material-icons">schedule</i><?php echo $post_add_date; ?></span>
				<?php if ( $post_add_date !== $post_edit_date ): ?>
                    <span title="Дата обновления"><i class="material-icons">update</i><?php echo $post_edit_date; ?></span>
				<?php endif; ?>
            </div>
			<?php if ( ! empty( $toc_content ) ): ?>
                <div class="col-auto post-toc-button">
                    <button>
                        <i class="material-icons">toc</i>
                    </button>
                </div>
			<?php endif; ?>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <article id="start">
					<?php the_content(); ?>
					<?php
					if ( is_single( 'hotkeys' ) ) {
						include_once 'inc/hotkeys.php';
					}
					?>
                </article>
            </div>
			<?php
			// если в правом сайдбаре нечего выводить (структура + похожие новости) ->
			// убираем его из вывода
			if ( !empty( $toc_content ) OR !empty( $category_top_posts ) ) :
				?>
                <div class="col-auto post-sidebar<?php echo $sb_none; ?>">
                    <div class="post-sidebar-container">
                        <button type="button" class="close d-block d-xl-none" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div class="toc">
                            <div class="toc-header d-lg-none">Содержание:</div>
                            <div class="toc-body">
								<?php echo $toc_content ?>
                            </div>
                        </div>

						<?php
						// проверка, добавлена ли ссылка на видео версию статьи
						// если да, то отображаем блок с ссылкой на ролик
						$get_current_post_fields = get_current_post_ACF_fields();
						if ( ! empty( $get_current_post_fields['youtube_version'] ) ):
							?>
                            <div class="video-version">
                                <div class="toc-title">Видео версия</div>
                                <div class="title-devider"></div>
                                <a href="https://www.youtube.com/watch?v=<?php echo $get_current_post_fields['youtube_version']; ?>" target="_blank">
                                    <img src="<?php echo get_template_directory_uri() . '/img/tube.png'; ?>" alt="видео версия статьи">
                                </a>
                            </div>
						<?php endif; ?>

						<?php if ( ! empty( $category_top_posts ) ): ?>
                            <div class="top-posts">
                                <div class="toc-title">Интересное</div>
                                <div class="title-devider"></div>
                                <ul>
									<?php foreach ( $category_top_posts as $top_post ): ?>
                                        <li>
                                            <a href="<?php the_permalink( $top_post->ID ); ?>"><?php echo $top_post->post_title; ?></a>
                                        </li>
									<?php endforeach; ?>
                                </ul>
                            </div>
						<?php endif; ?>
                    </div>
                </div>
			<?php endif; ?>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col">
                <div class="rating-block">
					<?php if(function_exists('plr_rating')) { plr_rating(); }; ?>
                </div>
            </div>
            <div class="col-auto">
                yandex share buttons
            </div>
        </div>
    </div>
</div>

<?php
if ( comments_open() || '0' != get_comments_number() ) {
	comments_template();
}
?>
