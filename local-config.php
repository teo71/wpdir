<?php
define( 'DB_NAME', 'wp_db_name' );
define( 'DB_USER', 'wp_db_user' );
define( 'DB_PASSWORD', 'wp_db_password' );
define( 'DB_HOST', 'mysql' );

define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );